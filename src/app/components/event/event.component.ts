import { Component, OnInit } from '@angular/core';
import { Event } from '@models/event.model';
import { ActivatedRoute } from '@angular/router';
import { EventsService } from '@services/events.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  event: Event;
  errMsg: string;
  baseUrl: string;
  mapsUrl = 'https://www.google.com/maps/';

  constructor(
    private route: ActivatedRoute,
    private events: EventsService
  ) {
    this.baseUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.events.getEvent(id).subscribe(
      event => this.event = event,
      errMsg => this.errMsg = errMsg
    );
  }

}
