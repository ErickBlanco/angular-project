import { Component, OnInit } from '@angular/core';
import { Event } from '@models/event.model';
import { EventsService } from '@services/events.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  events: Event[];
  errMsg: string;
  baseUrl: string;

  constructor(
    private eventService: EventsService
  ) {
    this.baseUrl = environment.apiUrl;
  }

  ngOnInit(): void {
    this.eventService.getEvents().subscribe(
      events => this.events = events,
      errMsg => this.errMsg = errMsg
    )
  }

}
