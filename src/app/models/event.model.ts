export interface Event {
  id: number;
  name: string;
  description: string;
  location: Location;
  rating: number;
  image: string;
  date: string;
  entranceCost: number;
  contactInfo: ContactInfo;
}

export interface ContactInfo {
  tel: string;
  email: string;
}

export interface Location {
  name: string;
  lat: number;
  lng: number;
}
