import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpMessageService } from './http-message.service';
import { Observable } from 'rxjs';
import { Event } from '@models/event.model';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  baseUrl: string;

  constructor(
    private http: HttpClient,
    private messageService: HttpMessageService
  ) { 
    this.baseUrl = environment.apiUrl;
  }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.baseUrl + 'events')
      .pipe(catchError(this.messageService.handleError));
  }

  getEvent(id: number | string): Observable<Event> {
    return this.http.get<Event>(this.baseUrl + 'events/' + id)
      .pipe(catchError(this.messageService.handleError));
  }
}
